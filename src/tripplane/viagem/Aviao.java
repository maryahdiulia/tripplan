package tripplane.viagem;

import java.util.Scanner;
import tripplane.passageiro.Passageiro;

public class Aviao {

    private Passageiro[][] lugares;

    /**
     * Este método cria as poltronas do avião sem passageiros, ou seja, todas as posições são preenchidas com null neste momento.
     */
    public void criaPoltronas() {
        lugares = new Passageiro[10][4];
        for (int l = 0; l < lugares.length; l++) {
            for (int c = 0; c < lugares[l].length; c++) {
                lugares[l][c] = null;
            }
        }
    }
    
    /**
     * Cria um avião e automaticamente chama o método que cria as poltronas do avião.
     */
    public Aviao() {
        this.criaPoltronas();
    }

    /**
     * Este método escolhe as poltronas
     *
     * @param p Passageiro que será inserido na poltrona do avião
     */
    public void escolherPoltrona(Passageiro p) {
        Scanner s = new Scanner(System.in);
        System.out.println("Selecione a fileira desejada.");
        int fileira = Integer.parseInt(s.nextLine());
        if (fileira > lugares.length - 1) {
            System.out.println("Fileira inexistente, selecione fileira disponível");
            this.escolherPoltrona(p);
            return;
        }
        System.out.println("Selecione a poltrona desejada.");

        int poltrona = Integer.parseInt(s.nextLine());
        this.verificaPoltrona(fileira, poltrona, p);
    }

    /**
     * Este método mostra as poltronas e a situação das mesmas (se estão
     * ocupadas ou disponíveis).
     */
    public void showPoltronas() {
        for (int linha = 0; linha < lugares.length; linha++) {
            System.out.println("------- Fileira " + linha + " -------");
            for (int c = 0; c < lugares[linha].length; c++) {
                if (lugares[linha][c] == null) {
                    System.out.println("Poltrona: " + c + " está disponível");
                } else {
                    System.out.println("Poltrona: " + c + " está ocupada por " + lugares[linha][c].getNome());
                }
            }

        }
    }

    /**
     * Este método verifica se a poltrona selecionada está disponível
     *
     * @param fileira seleciona uma fileira.
     * @param poltrona verifica se a poltrona na fileira selecionada está disponível.
     * @param p caso dísponivel, adiciona o passageiro naquela posição.
     */
    private void verificaPoltrona(int fileira, int poltrona, Passageiro p) {
        if (lugares[fileira][poltrona] != null) {
            System.out.println("A poltrona está ocupada por " + lugares[fileira][poltrona].getNome());
            this.escolherPoltrona(p);
        } else {
            lugares[fileira][poltrona] = p;
        }
    }

        /**
         * Este método remove um passageiro.
         * @param codigo é um parametro que será utilizado para verificar se há um passageiro pelo código digitado na hora da inserção do passageiro no sistema
         */
    public void removerPassageiro(int codigo) {
        int fileira = 0;
        int poltrona = 0;
        for (fileira = 0; fileira < lugares.length; fileira++) {
            for (poltrona = 0; poltrona < lugares[fileira].length; poltrona++) {
                if (lugares[fileira][poltrona] != null && lugares[fileira][poltrona].getCodigo() == codigo) {
                    lugares[fileira][poltrona] = null;
                    System.out.println("PASSAGEIRO REMOVIDO COM SUCESSO");
                    return;//se encontrou remove e volta para o menu
                }
            }
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n NÃO EXISTE PASSAGEIRO A SER REMOVIDO\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }

    /**
     * verifica onde está o passageiro de acordo com o codigo dele
     * @param codigo int utilizado para a verificação
     * @return  retorna se há ou não um passageiro naquela posição
     */
    public boolean getPassageiroPorCodigo(int codigo) {
        int fileira = 0;
        int poltrona = 0;
        for (fileira = 0; fileira < lugares.length; fileira++) {
            for (poltrona = 0; poltrona < lugares[fileira].length; poltrona++) {
                if (lugares[fileira][poltrona] != null && lugares[fileira][poltrona].getCodigo() == codigo) {
                    System.out.println("O passageiro "+ lugares[fileira][poltrona].getNome() +" está na fileira " + fileira + " poltrona " + poltrona + " do percurso:");
                    return true;//se encontrou exibe na tela
                }
            }
        }
        return false;
    }
}
