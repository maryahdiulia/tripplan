package tripplane.viagem;

import tripplane.viagem.Aviao;

import java.util.Scanner;
import tripplane.passageiro.Passageiro;

public class Percurso {

    String[] percursos = {"Sao Paulo -> Florianopolis", "Sao Paulo -> Porto Alegre", "Florianopolis -> São Paulo", "Florianopolis -> Porto Alegre", "Porto Alegre -> Florianópolis", "Porto Alegre -> São Paulo"};
    Scanner s = new Scanner(System.in);
    int escolha;
    /**
     * Cria um avião para cada percurso, para ser usado no momento de adição de
     * passageiros.
     */
    Aviao[] avioes = {new Aviao(), new Aviao(), new Aviao(), new Aviao(), new Aviao(), new Aviao()};

    //get
    public String[] getPercursos() {
        return this.percursos;
    }

    /**
     * Exibe na tela quais as opções de percurso e permite que seja selecionada
     * uma opção
     *
     * @param p passageiro a ser adicionado naquele percurso
     */
    public void criaPercursos(Passageiro p) {
        System.out.println("Opções de  percursos:");
        for (int i = 0; i < percursos.length; i++) {
            System.out.println(i + " : " + percursos[i]);
        }
        System.out.println("Digite o código do percurso escolhido: ");
        escolha = Integer.parseInt(s.nextLine());
        this.escolhePercurso(p);
    }

    /**
     * Exibe as poltronas disponiveis de acordo com o percurso escolhido e
     * adiciona o passageiro na posição escolhida
     *
     * @param p passageiro a ser adicionado naquela poltrona
     */
    public void escolhePercurso(Passageiro p) {
        System.out.println("Escolha sua poltrona para o percurso" + percursos[escolha]);
        switch (escolha) {
            case 0:
                avioes[0].showPoltronas();
                avioes[0].escolherPoltrona(p);
                break;
            case 1:
                avioes[1].showPoltronas();
                avioes[1].escolherPoltrona(p);
                break;
            case 2:
                avioes[2].showPoltronas();
                avioes[2].escolherPoltrona(p);
                break;
            case 3:
                avioes[3].showPoltronas();
                avioes[3].escolherPoltrona(p);
                break;
            case 4:
                avioes[4].showPoltronas();
                avioes[4].escolherPoltrona(p);
                break;
            case 5:
                avioes[5].showPoltronas();
                avioes[5].escolherPoltrona(p);
                break;
            case 6:
                avioes[6].showPoltronas();
                avioes[6].escolherPoltrona(p);
            default:
                System.out.println("OPÇÂO INVÁLIDA! \nDigite o código do percurso escolhido: ");
                criaPercursos(p);
                break;
        }
    }

    /**
     * Solicita o percurso do passageiro para fazer a verificação para remoção
     * do mesmo
     *
     * @param codigo int utilizado para verificação
     */
    public void consultaPercursoRemovePassageiro(int codigo) {
        Scanner s = new Scanner(System.in);
        System.out.println("Qual o percurso do passageiro?");
        for (int i = 0; i < percursos.length; i++) {
            System.out.println(i + " : " + percursos[i]);
        }
        escolha = Integer.parseInt(s.nextLine());
        Aviao v = avioes[escolha];
        v.removerPassageiro(codigo);
    }

    /**
     * Consulta um passageiro pelo codigo, exibindo na tela as informações como
     * percurso, poltrona e nome
     *
     * @param codigo
     */
    public void consultaPassageiro(int codigo) {
        for (int i = 0; i < percursos.length; i++) {
            Aviao v = avioes[i];
            if (v.getPassageiroPorCodigo(codigo)) {
                System.out.println(percursos[i]);
            }
        }
    }
}
