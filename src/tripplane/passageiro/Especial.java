package tripplane.passageiro;


import tripplane.passageiro.Passageiro;


public class Especial extends Passageiro
{
    private String necEspecial;
    private String acompanhante;
    
    //gets
    public String getNecEspecial(){
        return this.necEspecial;
    }
    
    public String getAcompanhante(){
        return this.acompanhante;
    }
    
    public Especial(String nome, int codigo, String email, String necEspecial, String acompanhante){
        super(nome,codigo,email);
        this.necEspecial = necEspecial;
        this.acompanhante = acompanhante;
    }
}
