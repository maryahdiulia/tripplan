package tripplane.passageiro;


import tripplane.passageiro.Passageiro;

public class Menores extends Passageiro
{
    private int idade;
    private String responsavel;
    
    //gets
    public int getIdade(){
        return this.idade;
    }

    public String getResponsavel(){
        return this.responsavel;
    }

    public Menores(String nome, int codigo, String email, int idade, String responsavel){
        super(nome,codigo,email);
        this.idade= idade;
        this.responsavel = responsavel;
    }   
}
