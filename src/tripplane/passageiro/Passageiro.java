package tripplane.passageiro;


/**
 * Escreva a descrição da classe Passageiro aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class Passageiro
{
    protected String nome; 
    protected int codigo;
    protected String email;
    
    //gets
    public String getNome(){
        return this.nome;
    }
    
    public int getCodigo(){
        return this.codigo;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    //construtor
    public Passageiro(String nome, int codigo, String email){
        this.nome = nome;
        this.codigo = codigo;
        this.email = email;
    }
    
}
