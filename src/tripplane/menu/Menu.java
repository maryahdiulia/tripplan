package tripplane.menu;

import tripplane.viagem.Percurso;
import java.util.Scanner;
import tripplane.passageiro.Passageiro;
import tripplane.passageiro.Especial;
import tripplane.passageiro.Menores;

public class Menu {

    private int escolha;
    Percurso percurso = new Percurso();

    /**
     * Esse método exibe o menu
     */
    public void exibirMenu() {
        Scanner s = new Scanner(System.in);
        System.out.println("GERENCIAMENTO DE PASSAGEIROS");
        System.out.println("----------------------------");
        System.out.println("1 - Inserir passageiro");
        System.out.println("2 - Remover passageiro");
        System.out.println("3 - Consultar passageiro");
        System.out.println("4 - Sair");
        System.out.println("Digite o numero correspondente a sua escolha: \n");
        escolha = Integer.parseInt(s.nextLine());
        menu();
    }

    /**
     * Esse método faz com que as opções exibidas no menu sejam funcionalizadas.
     */
    public void menu() {
        switch (escolha) {
            case 1:
                inserirPassageiro();
                break;
            case 2:
                removerPassageiro();
                break;
            case 3:
                consultarPassageiro();
                break;
            case 4:
                System.out.println("OBRIGADA POR UTILIZAR NOSSO SISTEMA! :)");
                break;
            default:
                System.out.println("Digite um numero valido: ");
                exibirMenu();
                break;
        }

    }

    /**
     * Esse método cria um passageiro e arquiva as informações
     *
     * @return retorna um novo passageiro com todas as informações que seram
     * utilizadas em outros métodos.
     */
    public Passageiro criarPassageiro() {
        Scanner s = new Scanner(System.in);
        System.out.println("Digite o nome do Passageiro:");
        String nome = s.nextLine();
        System.out.println("Digite o código do passageiro: (somente números inteiros)");
        int codigo = Integer.parseInt(s.nextLine());
        System.out.println("Digite o e-mail do passageiro:");
        String email = s.nextLine();
        return new Passageiro(nome, codigo, email);
    }

    //esse método cria um passageiro especial
    public Especial criarPassageiroEspecial() {
        Scanner s = new Scanner(System.in);
        System.out.println("Digite o nome do Passageiro:");
        String nome = s.nextLine();
        System.out.println("Digite o código do passageiro:");
        int codigo = Integer.parseInt(s.nextLine());
        System.out.println("Digite o e-mail do passageiro:");
        String email = s.nextLine();
        System.out.println("Digite a necessidade especial do passageiro:");
        String necEspecial = s.nextLine();
        System.out.println("Digite o nome do acompanhante:");
        String acompanhante = s.nextLine();
        return new Especial(nome, codigo, email, necEspecial, acompanhante);
    }

//esse método cria um passageiro menor
    public Menores criarPassageiroMenor() {
        Scanner s = new Scanner(System.in);
        System.out.println("Digite o nome do Passageiro:");
        String nome = s.nextLine();
        System.out.println("Digite o código do passageiro:");
        int codigo = Integer.parseInt(s.nextLine());
        System.out.println("Digite o e-mail do passageiro:");
        String email = s.nextLine();
        System.out.println("Digite a idade:");
        int idade = Integer.parseInt(s.nextLine());
        System.out.println("Digite o nome do responsável: ");
        String responsavel = s.nextLine();
        return new Menores(nome, codigo, email, idade, responsavel);
    }

    /**
     * Esse método insere um passageiro no avião e percurso escolhido.
     */
    public void inserirPassageiro() {
        Scanner s = new Scanner(System.in);
        System.out.println("Digite o número correspondente ao tipo de passageiro: ");
        System.out.println("1 - O passageiro é menor de idade.");
        System.out.println("2 - O passageiro é especial.");
        System.out.println("3 - Passageiro padrão.");
        int pass = Integer.parseInt(s.nextLine());
        switch (pass) {
            case 1:
                Menores m = this.criarPassageiroMenor();
                percurso.criaPercursos(m);
                break;
            case 2:
                Especial e = this.criarPassageiroEspecial();
                percurso.criaPercursos(e);
                break;
            case 3:
                Passageiro p = this.criarPassageiro();
                percurso.criaPercursos(p);
                break;
            default:
                System.out.println("Digite uma opção válida");
                inserirPassageiro();
        }

        System.out.println("***********************************\n PASSAGEIRO ADICIONADO COM SUCESSO! \n***********************************\n");
        this.exibirMenu();
    }

    /**
     * Esse método exclui um passageiro do avião e do percurso escolhido.
     */
    public void removerPassageiro() {
        Scanner s = new Scanner(System.in);
        System.out.println("Qual o código do passageiro a ser removido?");
        int codigo = Integer.parseInt(s.nextLine());
        percurso.consultaPercursoRemovePassageiro(codigo);
        this.exibirMenu();

    }

    /**
     * Esse método consulta dados do passageiro no avião e percurso escolhido.
     */
    public void consultarPassageiro() {
        Scanner s = new Scanner(System.in);
        System.out.println("Digite o codigo do passageiro a ser consultado:");
        int codigo = Integer.parseInt(s.nextLine());
        percurso.consultaPassageiro(codigo);
        this.exibirMenu();
    }
}
